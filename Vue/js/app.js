apiURL = 'http://rest.movies.dev/api/';

new Vue({
    el: '#app',
    data:{
        movies: '',
        movie: '',
        genres: '',
        liveFilter: '',
        genreFilter: ''
    },
    ready: function(){
        this.getMovies();
        this.getGenres();
    },
    methods: {
        getMovies: function(){
            this.$http.get(apiURL+'movies',function(movies){
                this.$set('movies',movies);
            });
        },
        getGenres: function(){
            this.$http.get(apiURL+'genres',function(genres){
                this.$set('genres',genres);
            });
        },
        getTheMovie: function(id){
            this.$http.get(apiURL+'movies/'+id,function(movie){
                this.$set('movie',movie);
            });
        }

    }
});